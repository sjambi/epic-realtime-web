package models
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.validation.ValidationError

case class Keyword(
	// event: String, 
	text: String//,
	// active: Boolean
	)

object Keyword {
  implicit val KeywordFormat: Format[Keyword] = Json.format[Keyword]
  // implicit val KeywordDataReads = (
	 //  	// (__ \ 'event).read[String] and
	 //  	(__ \ 'text).read[String] //and
	 //  	// (__ \ 'active).read[Boolean] 
  // 	)(Keyword.apply _)
}
