package models
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.validation.ValidationError

case class Query(
	name: String, 
	event: String, 
	query_type: String,
	attribute: String, 
	lowerTime: String, 
	upperTime: String, 
	trendType: String, 
	value: String, 
	running: Boolean,
	job_id: String
	)

object Query {
  implicit val QueryFormat: Format[Query] = Json.format[Query]
  // implicit val QueryDataReads = (
  // 	    (__ \ 'name).read[String] and
	 //  	(__ \ 'event).read[String] and
	 //  	(__ \ 'query_type).read[String] and
	 //  	(__ \ 'attribute).read[String] and
	 //  	(__ \ 'lowerTime).read[String] and
	 //  	(__ \ 'upperTime).read[String] and
	 //  	(__ \ 'trendType).read[String] and
	 //  	(__ \ 'value).read[String] and
	 //  	(__ \ 'running).read[Boolean] and
	 //  	(__ \ 'job_id).read[String]
  // 	)(Query.apply _)
}

case class CreateQueryForm(
	query_type: String,
	date: String, 
	ld1: String, 
	ud1: String,
	ld2: String, 
	ud2: String,
	ld3: String, 
	ud3: String,
	trend: String,
	k: String, 
	t: String
)
